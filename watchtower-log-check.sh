#!/bin/bash
#
# Outputting the Watchtower container logs to an external log file.

## Get current date ##
_now=$(date +"%m_%d_%Y")

## Appending a current date from a $_now to a filename stored in $_file ##
_file="/usr/src/data/docker-checks/watchtower/watchtower_$_now.txt"

echo "Starting health checks on Docker and outputting the results to $_file."
docker logs watchtower &> "$_file"

echo "Now we're done!"