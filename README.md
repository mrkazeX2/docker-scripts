# Docker Scripts

These are soon small scripts I use to automate checking certain tasks on my home lab. I figure these would be useful for someone else. You can freely use the "helath checker" script as long as you have Docker installed. The Watchtower script will require a configured Watchtower container to be running and obviously Docker preinstalled.