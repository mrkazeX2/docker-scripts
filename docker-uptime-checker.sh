#!/bin/bash
#
# Run basic docker health checks!

## Get current date ##
_now=$(date +"%m_%d_%Y")

## Appending a current date from a $_now to a filename stored in $_file ##
_file="/filepath-here/health-checks_$_now.txt"

echo "Starting health checks on Docker and outputting the results to $_file."
docker ps -a | grep Up >> "$_file"

echo "Now we're done!"